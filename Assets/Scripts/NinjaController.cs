using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
   private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public GameObject rightBullet;
    public GameObject leftBullet;
    public bool escalar = false;
    public bool enTierra = false;
    public bool glide = false;
    public float distancia = 0;
    private GameController _game;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);
        if (!enTierra)
        {
            if (rb.velocity.y < 0)
            {
                distancia += Time.deltaTime;
            }
        }
        glide = false;
        rb.velocity = new Vector2(0, rb.velocity.y); 

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(6, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-6, rb.velocity.y); 
            sr.flipX = true; 
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * 40, ForceMode2D.Impulse);
            
            animator.SetInteger("Estado",2); 
        }   
        if (Input.GetKey(KeyCode.C))
        {
            animator.SetInteger("Estado",6);
            glide = true;

        } 
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetInteger("Estado",4); 
        } 
        if (Input.GetKeyUp(KeyCode.X))
        {
            animator.SetInteger("Estado",5); 
            var bullet = sr.flipX ? leftBullet : rightBullet;
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = rightBullet.transform.rotation;
            Instantiate(bullet, position, rotation);
            
        }
        if (escalar)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0); 
        }
        if (Input.GetKey(KeyCode.UpArrow) && escalar)
        {
            animator.SetInteger("Estado", 3);
            rb.velocity = new Vector2(rb.velocity.x, 3); 
        }
        if (Input.GetKey(KeyCode.DownArrow) && escalar)
        {
            animator.SetInteger("Estado", -3);
            rb.velocity = new Vector2(rb.velocity.x, -3); 
        }
        
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("escalera"))
        {
            escalar = true;
            rb.gravityScale = 0;
        }
        
        
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("escalera"))
        {
            escalar = false;
            rb.gravityScale = 8;
        }
        
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("floor"))
        {
            enTierra = true;
            Debug.Log(distancia);
            if (distancia>=0.6 && glide == false && escalar == false)
            {
               Destroy(this.gameObject);
            }
            distancia=0;

        }
         if (collision.gameObject.CompareTag("enemy"))
        {
            
            _game.LoseLife();
            if (_game.GetLifes()==0)
            {
                Destroy(this.gameObject);
            }
            
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("floor"))
        {
            enTierra = false;

        }
    }

}