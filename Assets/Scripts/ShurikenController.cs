using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenController : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    
    private GameController _game;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
        Destroy(this.gameObject, 3);
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.flipX)
        {
            rb.velocity = new Vector2(-6, rb.velocity.y);
        }else
        {
            rb.velocity = new Vector2(6, rb.velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("enemy"))
        {
            Destroy(this.gameObject);
            Destroy(other.gameObject);
            _game.PlusScore(10);
        }
        
    }
}


