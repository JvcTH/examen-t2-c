using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesController : MonoBehaviour
{
   public GameObject zombie;
    public float generar = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        generar += Time.deltaTime;
        if (generar > 5)
        {
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = zombie.transform.rotation;
            Instantiate(zombie, position, rotation);
            generar=0;
        }
        
    }
}